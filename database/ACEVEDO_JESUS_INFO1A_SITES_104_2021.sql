-- OM 2021.02.17
-- FICHIER MYSQL POUR FAIRE FONCTIONNER LES EXEMPLES
-- DE REQUESTS MYSQL
-- Database: ACEVEDO_JESUS_INFO1A_SITES_104_2021

-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF EXISTS ACEVEDO_JESUS_INFO1A_SITES_104_2021;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS ACEVEDO_JESUS_INFO1A_SITES_104_2021;

-- Utilisation de cette base de donnée

USE ACEVEDO_JESUS_INFO1A_SITES_104_2021;
-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Structure de la table `t_avoir_mail`
--

CREATE TABLE `t_avoir_mail` (
  `id_avoir_mail` int(11) NOT NULL,
  `fk_emplacements` int(50) NOT NULL,
  `fk_mail` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_avoir_materiel`
--

CREATE TABLE `t_avoir_materiel` (
  `id_avoir_materiel` int(11) NOT NULL,
  `fk_emplacements` int(11) NOT NULL,
  `fk_materiel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_avoir_telephone`
--

CREATE TABLE `t_avoir_telephone` (
  `id_avoir_telephone` int(11) NOT NULL,
  `fk_emplacements` int(11) NOT NULL,
  `fk_telephone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_emplacements`
--

CREATE TABLE `t_emplacements` (
  `id_emplacements` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `adresse` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_emplacements`
--

INSERT INTO `t_emplacements` (`id_emplacements`, `nom`, `adresse`) VALUES
(1, 'LGF-066-Berne-Papiermühlestrasse 83', 'Papiermühlestrasse 83, Bern'),
(2, 'LGF-029-Vevey-Av. Général-Guisan 31', 'Avenue du Général-Guisan 31, Vevey');

-- --------------------------------------------------------

--
-- Structure de la table `t_mail`
--

CREATE TABLE `t_mail` (
  `id_mail` int(11) NOT NULL,
  `nom_mail` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_mail`
--

INSERT INTO `t_mail` (`id_mail`, `nom_mail`) VALUES
(1, 'jacevedo@neoadvertising'),
(2, 'jack@neoadvertising.com'),
(3, 'francois@neoadvertising.com');

-- --------------------------------------------------------

--
-- Structure de la table `t_materiel`
--

CREATE TABLE `t_materiel` (
  `id_materiel` int(11) NOT NULL,
  `type_materiel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_materiel`
--

INSERT INTO `t_materiel` (`id_materiel`, `type_materiel`) VALUES
(1, 'Player'),
(2, 'PDU'),
(3, 'rPi'),
(4, 'Switch');

-- --------------------------------------------------------

--
-- Structure de la table `t_personne`
--

CREATE TABLE `t_personne` (
  `id_personne` int(11) NOT NULL,
  `nom_pers` varchar(42) NOT NULL,
  `prenom_pers` varchar(42) NOT NULL,
  `date_naissance_pers` date NOT NULL,
  `taille_pers` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_personne`
--

INSERT INTO `t_personne` (`id_personne`, `nom_pers`, `prenom_pers`, `date_naissance_pers`, `taille_pers`) VALUES
(1, 'Acevedo', 'Jesus', '2001-04-15', 100),
(2, 'Acevedo', 'Jesus', '2021-02-01', 100),
(3, 'Acevedo', 'Jesus', '2021-02-01', 100);

-- --------------------------------------------------------

--
-- Structure de la table `t_telephone`
--

CREATE TABLE `t_telephone` (
  `id_telephone` int(11) NOT NULL,
  `numero_tel` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_telephone`
--

INSERT INTO `t_telephone` (`id_telephone`, `numero_tel`) VALUES
(1, 795803345),
(2, 785883345);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_avoir_mail`
--
ALTER TABLE `t_avoir_mail`
  ADD PRIMARY KEY (`id_avoir_mail`),
  ADD KEY `fk_emplacements` (`fk_emplacements`),
  ADD KEY `fk_mail` (`fk_mail`);

--
-- Index pour la table `t_avoir_materiel`
--
ALTER TABLE `t_avoir_materiel`
  ADD PRIMARY KEY (`id_avoir_materiel`),
  ADD KEY `fk_emplacements` (`fk_emplacements`),
  ADD KEY `fk_materiel` (`fk_materiel`);

--
-- Index pour la table `t_avoir_telephone`
--
ALTER TABLE `t_avoir_telephone`
  ADD PRIMARY KEY (`id_avoir_telephone`),
  ADD KEY `fk_emplacements` (`fk_emplacements`),
  ADD KEY `fk_telephone` (`fk_telephone`);

--
-- Index pour la table `t_emplacements`
--
ALTER TABLE `t_emplacements`
  ADD PRIMARY KEY (`id_emplacements`);

--
-- Index pour la table `t_mail`
--
ALTER TABLE `t_mail`
  ADD PRIMARY KEY (`id_mail`);

--
-- Index pour la table `t_materiel`
--
ALTER TABLE `t_materiel`
  ADD PRIMARY KEY (`id_materiel`);

--
-- Index pour la table `t_personne`
--
ALTER TABLE `t_personne`
  ADD PRIMARY KEY (`id_personne`);

--
-- Index pour la table `t_telephone`
--
ALTER TABLE `t_telephone`
  ADD PRIMARY KEY (`id_telephone`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_avoir_mail`
--
ALTER TABLE `t_avoir_mail`
  MODIFY `id_avoir_mail` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_avoir_materiel`
--
ALTER TABLE `t_avoir_materiel`
  MODIFY `id_avoir_materiel` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_avoir_telephone`
--
ALTER TABLE `t_avoir_telephone`
  MODIFY `id_avoir_telephone` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `t_emplacements`
--
ALTER TABLE `t_emplacements`
  MODIFY `id_emplacements` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `t_mail`
--
ALTER TABLE `t_mail`
  MODIFY `id_mail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `t_materiel`
--
ALTER TABLE `t_materiel`
  MODIFY `id_materiel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `t_personne`
--
ALTER TABLE `t_personne`
  MODIFY `id_personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `t_telephone`
--
ALTER TABLE `t_telephone`
  MODIFY `id_telephone` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_avoir_mail`
--
ALTER TABLE `t_avoir_mail`
  ADD CONSTRAINT `t_avoir_mail_ibfk_1` FOREIGN KEY (`fk_emplacements`) REFERENCES `t_emplacements` (`id_emplacements`),
  ADD CONSTRAINT `t_avoir_mail_ibfk_2` FOREIGN KEY (`fk_mail`) REFERENCES `t_mail` (`id_mail`);

--
-- Contraintes pour la table `t_avoir_materiel`
--
ALTER TABLE `t_avoir_materiel`
  ADD CONSTRAINT `t_avoir_materiel_ibfk_1` FOREIGN KEY (`fk_emplacements`) REFERENCES `t_emplacements` (`id_emplacements`),
  ADD CONSTRAINT `t_avoir_materiel_ibfk_2` FOREIGN KEY (`fk_materiel`) REFERENCES `t_materiel` (`id_materiel`);

--
-- Contraintes pour la table `t_avoir_telephone`
--
ALTER TABLE `t_avoir_telephone`
  ADD CONSTRAINT `t_avoir_telephone_ibfk_1` FOREIGN KEY (`fk_emplacements`) REFERENCES `t_emplacements` (`id_emplacements`),
  ADD CONSTRAINT `t_avoir_telephone_ibfk_2` FOREIGN KEY (`fk_telephone`) REFERENCES `t_telephone` (`id_telephone`);


